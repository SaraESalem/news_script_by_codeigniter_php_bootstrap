-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 06:33 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `news_script`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'arts'),
(2, 'sports'),
(4, 'political'),
(12, 'space'),
(13, 'science'),
(14, 'travels'),
(15, 'tttttttt'),
(16, 'qqqqqq');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`id` int(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `text` text NOT NULL,
  `img` varchar(200) NOT NULL,
  `writer_id` int(20) NOT NULL,
  `cat_id` int(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `img`, `writer_id`, `cat_id`) VALUES
(13, 'Palmyra: ''Mass grave'' in recaptured Syrian city', 'The grave containing the bodies of men, women and children was uncovered on the north-eastern edge of the city.\r\nSome had been beheaded while others had been shot, a military source told AFP.\r\nHe said they were officers, soldiers, members of pro-regime militia and their relatives.\r\nTwenty-four of the victims were civilians, including three children, the source added.', '_89049454_89049449.jpg', 1, 4),
(14, 'Anti-mosque banner at Aussie Rules game condemned', 'The banner - seen during the game between Collingwood and Richmond - also featured the insignia of the United Patriots Front, a far right group.\r\nIt was eventually taken down and the fans responsible removed.\r\nThe AFL has previously seen racism rows over the widespread booing of an Aboriginal player.', '_89048303_89048302.jpg', 1, 2),
(15, 'Essex ''Van'' Gogh makes art on mucky vans to ''make drivers smile''', 'The artist, known only as Mr Konjusha is 22 and from east London.\r\nHis work has been spotted at various locations since he started drawing on the vehicles about three weeks ago. He said he had worked on 10 vans so far.\r\nMr Konjusha said he was doing this to "put a smile" on delivery drivers'' faces. Each creation takes about five minutes, he said.', '_89042003_van2.jpg', 1, 1),
(16, 'Russia’s A5V heavy-lift rocket to be ‘ready for lunar missions by 2025’ – Roscosmos chief to RT', 'Russian engineers have presented a prototype of Russia’s most powerful “Angara A5V” heavy-lift rocket that may perform its first manned flight to the Moon by 2030. As RT has learned from the Roscosmos chief the project is proceeding according to schedule.', '56ff2e03c46188d8688b45d4.jpg', 1, 12),
(17, 'ISIS bomb & chemical weapons ‘research center’ in Mosul barely targeted in US strikes', 'ISIS has been using a high-tech lab in Mosul for building bombs and chemical weapons and training recruits in bomb-making since its capture in 2014. Barely targeted by US airstrikes, the facility has also provided jihadists with some 40 kg of uranium compounds.', '56feb27dc4618826458b45a0.jpg', 1, 12),
(18, 'Most European ISIS fighters come from France, Germany, UK – study', 'French, German and British citizens constitute the majority of the European foreign fighters that joined the ranks of Islamic State (IS, formerly ISIS/ISIL) in Syria and Iraq, the latest study by an independent research center based in The Hague says.', '56fed969c36188f87a8b45b5.jpg', 1, 14),
(22, 'yyyyyyyyyyy', 'qqqqqqqqqqqqqyyyyyyyyyyyy', '40815-girls-for-fb-profile-dp-cute-stylo-girl-for-dp-stylish-girls-for-fb.jpg', 19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `writer`
--

CREATE TABLE IF NOT EXISTS `writer` (
`id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `writer`
--

INSERT INTO `writer` (`id`, `name`, `email`, `password`) VALUES
(1, 'sarasalem', 'sara@yahoo.com', '51b7183f67e3f2fc30c7d089e5dffe45'),
(2, 'xyzsa', 'ee@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(3, 'voot', 'voo@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(4, 'sadaa', 'dev.sarasalem@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(5, 'xxxxx', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'ttttt', 'eng.sara1991@yahoo.com', 'd3eb9a9233e52948740d7eb8c3062d14'),
(7, 'eeeeeeee', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(8, 'ertw', 'ee@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(9, 'gggggg', 'magnum_team4@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(10, 'sadas', 'eng.sara1991@yahoo.com', 'fcea920f7412b5da7be0cf42b8c93759'),
(11, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(12, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(13, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(14, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(15, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(16, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(17, 'danas', 'sara@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(18, 'robyx', 'rob@gmail.com', '51b7183f67e3f2fc30c7d089e5dffe45'),
(19, 'noor', 'noor@yahoo.com', '51b7183f67e3f2fc30c7d089e5dffe45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`), ADD KEY `cat_id` (`cat_id`), ADD KEY `writer_id` (`writer_id`);

--
-- Indexes for table `writer`
--
ALTER TABLE `writer`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `writer`
--
ALTER TABLE `writer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `news`
--
ALTER TABLE `news`
ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `category` (`id`),
ADD CONSTRAINT `news_ibfk_2` FOREIGN KEY (`writer_id`) REFERENCES `writer` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
